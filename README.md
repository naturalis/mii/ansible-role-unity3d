Role Name
=========

This role is an extension of the interactive role.

Requirements
------------

To run this role you also need to install the interactive role.

Role Variables
--------------

`unity3d_content`

The name of the zip file containing the unity3d content.

`unity3d_executable`

Path to the executable of the unity3d content.

License
-------

BSD
